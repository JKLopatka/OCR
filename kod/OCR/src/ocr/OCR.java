package ocr;
import com.asprise.ocr.Ocr;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import com.asprise.ocr.OcrException;

/**
 *
 * @author Jagoda
 */
public class OCR 
{
    
    public static void write_to_file(String file_to_write, String text) throws IOException
    {

        BufferedWriter output = null;
        try {
            File file = new File(file_to_write);
            output = new BufferedWriter(new FileWriter(file));
            output.write(text);
        } catch ( IOException e ) {
        } finally {
          if ( output != null ) {
            output.close();
          }
        }
    }

    public static String made_ocr(String file_path, String lengua, String write_to) throws IOException 
    {      
        Ocr.setUp(); // ustawienie
        Ocr ocr = new Ocr(); // nowy silnik ocr
        ocr.startEngine(lengua, Ocr.SPEED_FASTEST); // starst silnika, jezyk, tryb   
        String s = ocr.recognize(new File[] {new File(file_path)},Ocr.RECOGNIZE_TYPE_ALL, Ocr.OUTPUT_FORMAT_PLAINTEXT);
        //metoda rozpoznawania ocr z pliku o podanej sciezce, dla wszystkich typów output jako string
        write_to_file(write_to,s);
           // do konsoli
        ocr.stopEngine();//zattrzymanie silnika
        return s;
    }
    
    public static String made_ocr(String file_path, String lengua) throws OcrException
    {   String s = null;
        try{
        Ocr.setUp(); // ustawienie
        Ocr ocr = new Ocr(); // nowy silnik ocr
        ocr.startEngine(lengua, Ocr.SPEED_FASTEST); // starst silnika, jezyk, tryb  
      
        s = ocr.recognize(new File[] {new File(file_path)},Ocr.RECOGNIZE_TYPE_ALL, Ocr.OUTPUT_FORMAT_PLAINTEXT);
        //metoda rozpoznawania ocr z pliku o podanej sciezce, dla wszystkich typów output jako string
        ocr.stopEngine();//zattrzymanie silnika
        }  catch (OcrException e){
            s = e.getMessage();
        }
        return s;
    }

}
