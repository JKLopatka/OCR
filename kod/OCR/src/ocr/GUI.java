/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ocr;

/**
 *
 * @author Jagoda
 */
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.text.Document;
import static ocr.OCR.made_ocr;

/**
 *
 * @author Jagoda
 */
public class GUI implements ActionListener {

    JFrame frame;
    JButton pathButton;
    JButton writeButton;
    JButton ocrButton;
    JPanel panel;
    JPanel panel1;
    JPanel panel2;
    JPanel panela;
    JPanel panelb;
    JPanel panelc;
    
    JTextPane jPicturePane;
    JTextArea jOCRText;
    JScrollPane jScrollPicture;
    JScrollPane jScrollAfterOCRText;
    JTextField file_path_box;
    JTextField file_write_box;
    String file_path = "";
    String file_write_path = "";
    String ocr;
    Icon image;
    
    JLabel jNameLabel;
    JLabel jlabel1;
    JLabel jlabel2;
    JLabel jlabel3;
    JLabel jlabel4;
    
    JRadioButton leng;
    JRadioButton lspa;
    JRadioButton ldeu;
    JRadioButton lfra;
    JRadioButton lpor;
    ButtonGroup group;
    JCheckBox chb;

    public static void main(String[] args) {
        GUI gui = new GUI();
        gui.startGUI();
    }

    public void startGUI() {
        
        frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setPreferredSize( new Dimension( 980, 700 ) );

        ocrButton = new JButton("WYKONAJ");
        
        panel = new JPanel();
        panel1 = new JPanel();
        panel2 = new JPanel();
        panela = new JPanel();
        panelb = new JPanel();
        panelc = new JPanel();
        
        jPicturePane = new JTextPane();
        jScrollPicture = new JScrollPane(jPicturePane);
   
        jOCRText = new JTextArea("");
        jScrollAfterOCRText = new JScrollPane(jOCRText);
        
       // file_path_box = new JTextField("skad");
        file_path_box = new JTextField("./testy/");
       // file_write_box = new JTextField("dokad");
        file_write_box = new JTextField("./testy/wyniki/");
        jNameLabel = new JLabel();
        
        leng = new JRadioButton("eng", true);
        lspa= new JRadioButton("spa");
        ldeu= new JRadioButton("due");
        lfra= new JRadioButton("fra");
        lpor= new JRadioButton("por"); 
        group = new ButtonGroup();
        group.add(leng);
        group.add(lspa);
        group.add(ldeu);
        group.add(lfra);
        group.add(lpor);
         
        chb = new JCheckBox("do pliku?");
    
        frame.add(panel, "North");
        frame.add(panel1);
        frame.add(panel2, "South");
       
        panela.setPreferredSize( new Dimension( 550, 60 ) );
        panelb.setPreferredSize( new Dimension( 300, 60 ) );
        panelc.setPreferredSize( new Dimension( 500, 50 ) );
        panel.setPreferredSize( new Dimension( 980, 40 ) );
        panel2.setPreferredSize( new Dimension( 980, 520 ) );
        jPicturePane.setPreferredSize( new Dimension( 450, 500 ) );
        jOCRText.setPreferredSize( new Dimension( 450, 500 ) );

        file_path_box.setColumns(40);       
        file_write_box.setColumns(40);

        jNameLabel.setFont(new java.awt.Font("Times New Roman", 2, 24));
        jNameLabel.setText(" OCR ");
   
        panel.add(jNameLabel);

        panel1.add(panela);
        panel1.add(panelb);
        panel1.add(panelc);
        
        panela.add(file_path_box, BorderLayout.SOUTH);
        panela.add(file_write_box,BorderLayout.NORTH);     
        
        panelb.add(leng);
        panelb.add(lspa);
        panelb.add(ldeu);
        panelb.add(lfra);
        panelb.add(lpor);
        panelb.add(chb);
        
        panelc.add(ocrButton, BorderLayout.EAST);
                
        panel2.add(jScrollPicture, BorderLayout.EAST);
        panel2.add(jScrollAfterOCRText, BorderLayout.WEST);

        ocrButton.addActionListener(this);
        
        frame.pack();
        frame.setVisible(true);
        panel.setVisible(true);
        jScrollPicture.setVisible(true);
        jScrollAfterOCRText.setVisible(true);
        jNameLabel.setVisible(true);

    }

    @Override
    public void actionPerformed(ActionEvent akcja) 
    {
        poczatek:
        if(akcja.getSource() == ocrButton)
        {
            file_path = file_path_box.getText();
            file_write_path = file_write_box.getText();
            String lengua = null;
            BufferedImage img = null;

            for (Enumeration<AbstractButton> buttons = group.getElements(); buttons.hasMoreElements();)
            {
                AbstractButton button = buttons.nextElement();
                if (button.isSelected())
                {
                    lengua =  button.getText();
                }
                if("skad".equals(file_path)){
                    jOCRText.setText("Wprowadz poprawna sciezke do pliku.");
                    break poczatek;
                }
            }
            try {
                if(isPDF(file_path)){
                    img = ImageIO.read(new File("./testy/reading_pdf.jpg"));
                }
                else{
                    try 
                    {
                         img = ImageIO.read(new File(file_path));
                    }
                    catch (IOException ex) 
                    {
                      Logger.getLogger(GUI.class.getName()).log(Level.SEVERE, null, ex);
                      jOCRText.setText("Błąd pliku wejściowego.");
                    }
                }
            } catch (FileNotFoundException ex) {
                Logger.getLogger(GUI.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(GUI.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            ImageIcon pictureImage = new ImageIcon(img);
            Image rs =((ImageIcon) pictureImage).getImage();
            ImageIcon con = new ImageIcon(img.getScaledInstance(jPicturePane.getWidth(), jPicturePane.getHeight(), Image.SCALE_AREA_AVERAGING));
            jPicturePane.insertIcon(con);
            
            if(chb.isSelected())
            {
                try 
                {
                    ocr = made_ocr(file_path,lengua,file_write_path);
                } 
                catch (IOException ex)
                {
                    Logger.getLogger(GUI.class.getName()).log(Level.SEVERE, null, ex);
                    jOCRText.setText("Błąd przetwarzania pliku.");
                }
            }
            else
            {
                ocr = made_ocr(file_path,lengua);
                if("".equals(ocr)){
                   jOCRText.setText("Blad przetwarzania pliku. \nPlik moze być pusty, nieczytelny lub w nieobslugiwanym jezyku.");
                }else{
                    jOCRText.setText(ocr);
                }
            }
        }
    }
            
    public boolean isPDF(String file_path) throws FileNotFoundException{
    File file = new File(file_path);
    Scanner input = new Scanner(new FileReader(file));
    while (input.hasNextLine()) {
        final String checkline = input.nextLine();
        if(checkline.contains("%PDF-")) { 
            return true;
        }  
    }
        return false;
    }
    
}

